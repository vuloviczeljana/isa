package app.repository;

import app.model.Banka;

import java.awt.print.Pageable;

import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;


@Repository
public interface BankaRepository extends PagingAndSortingRepository<Banka, Long>{ 


}
