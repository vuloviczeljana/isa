package app.repository;

import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import app.model.Klijent;

@Repository
public interface KlijentRepository extends PagingAndSortingRepository<Klijent, Long>{

}
