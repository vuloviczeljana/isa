package app.repository;

import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import app.model.Naplata;

@Repository
public interface NaplataRepository extends PagingAndSortingRepository<Naplata, Long>{

}
