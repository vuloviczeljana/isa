package app.controller;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import app.model.Klijent;
import app.service.KlijentService;
import app.service.KlijentService;

@Controller
@RequestMapping(path = "api/klijent")
public class KlijentController {
	@Autowired
	KlijentService klijentService;

	@RequestMapping(path = "/dobavi-sve", method = RequestMethod.GET)
	public ResponseEntity<Iterable<Klijent>> dobaviKlijente() {
		return new ResponseEntity<Iterable<Klijent>>(klijentService.findAll(), HttpStatus.OK);
	}

	@RequestMapping(path = "/{id}", method = RequestMethod.GET)
	public ResponseEntity<Optional<Klijent>> dobaviKlijenta(@PathVariable("id") Long id) {
		Optional<Klijent> n = klijentService.findOne(id);
		if (n != null)
			return new ResponseEntity<Optional<Klijent>>(n, HttpStatus.OK);
		else
			return new ResponseEntity<Optional<Klijent>>(HttpStatus.NOT_FOUND);
	}

	@RequestMapping(path = "", method = RequestMethod.POST)
	public ResponseEntity<Klijent> save(@RequestBody() Klijent n) {
		klijentService.save(n);
		return new ResponseEntity<Klijent>(n, HttpStatus.CREATED);
	}

	@RequestMapping(path = "/{id}", method = RequestMethod.DELETE)
	public ResponseEntity<Klijent> remove(@PathVariable("id") Long id) {
		Optional<Klijent> n = klijentService.findOne(id);
		if (n == null) {
			return new ResponseEntity<Klijent>(HttpStatus.NOT_FOUND);
		}
		klijentService.remove(id);
		return new ResponseEntity<Klijent>(HttpStatus.OK);
	}

	@RequestMapping(path = "", method = RequestMethod.PUT)
	public ResponseEntity<Optional<Klijent>> edit(@RequestBody() Klijent n) {
		Optional<Klijent> klijent = klijentService.findOne(n.getId());
		if (klijent == null) {
			return new ResponseEntity<Optional<Klijent>>(HttpStatus.NOT_FOUND);
		}
		klijentService.save(n);
		return new ResponseEntity<Optional<Klijent>>(HttpStatus.OK);
	}
}
