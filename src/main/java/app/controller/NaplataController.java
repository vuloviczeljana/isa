package app.controller;

import java.util.ArrayList;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import app.model.Banka;
import app.model.Naplata;
import app.service.BankaService;
import app.service.NaplataService;


@Controller
@RequestMapping(path="api/naplata")
public class NaplataController {

	@Autowired
	NaplataService naplataService;
	
	@RequestMapping(path = "/dobavi-sve", method = RequestMethod.GET)
	public ResponseEntity<Iterable<Naplata>> dobaviNaplatu() {
		return new ResponseEntity<Iterable<Naplata>>(naplataService.findAll(), HttpStatus.OK);
	}

	@RequestMapping(path = "/{id}", method = RequestMethod.GET)
	public ResponseEntity<Optional<Naplata>> dobaviNaplatu(@PathVariable("id") Long id) {
		Optional<Naplata> n = naplataService.findOne(id);
		if (n != null)
			return new ResponseEntity<Optional<Naplata>>(n, HttpStatus.OK);
		else
			return new ResponseEntity<Optional<Naplata>>(HttpStatus.NOT_FOUND);
	}

	@RequestMapping(path = "", method = RequestMethod.POST)
	public ResponseEntity<Naplata> save(@RequestBody() Naplata n) {
		naplataService.save(n);
		return new ResponseEntity<Naplata>(n, HttpStatus.CREATED);
	}

	@RequestMapping(path = "/{id}", method = RequestMethod.DELETE)
	public ResponseEntity<Naplata> remove(@PathVariable("id") Long id) {
		Optional<Naplata> n = naplataService.findOne(id);
		if (n == null) {
			return new ResponseEntity<Naplata>(HttpStatus.NOT_FOUND);
		}
		naplataService.remove(id);
		return new ResponseEntity<Naplata>(HttpStatus.OK);
	}

	@RequestMapping(path = "", method = RequestMethod.PUT)
	public ResponseEntity<Optional<Naplata>> edit(@RequestBody() Naplata n) {
		Optional<Naplata> naplata = naplataService.findOne(n.getId());
		if (naplata == null) {
			return new ResponseEntity<Optional<Naplata>>(HttpStatus.NOT_FOUND);
		}
		naplataService.save(n);
		return new ResponseEntity<Optional<Naplata>>(HttpStatus.OK);
	}
}
