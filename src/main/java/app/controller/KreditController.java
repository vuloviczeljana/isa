package app.controller;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import app.model.Banka;
import app.model.Kredit;
import app.service.KreditService;

@Controller
@RequestMapping(path="api/kredit")
public class KreditController {
	@Autowired
	KreditService kreditService;
	
	@RequestMapping(path = "/dobavi-sve", method = RequestMethod.GET)
	public ResponseEntity<Iterable<Kredit>> dobaviKredit() {
		return new ResponseEntity<Iterable<Kredit>>(kreditService.findAll(), HttpStatus.OK);
	}

	@RequestMapping(path = "/{id}", method = RequestMethod.GET)
	public ResponseEntity<Optional<Kredit>> dobaviKredit(@PathVariable("id") Long id) {
		Optional<Kredit> n = kreditService.findOne(id);
		if (n != null)
			return new ResponseEntity<Optional<Kredit>>(n, HttpStatus.OK);
		else
			return new ResponseEntity<Optional<Kredit>>(HttpStatus.NOT_FOUND);
	}

	@RequestMapping(path = "", method = RequestMethod.POST)
	public ResponseEntity<Kredit> save(@RequestBody() Kredit n) {
		kreditService.save(n);
		return new ResponseEntity<Kredit>(n, HttpStatus.CREATED);
	}

	@RequestMapping(path = "/{id}", method = RequestMethod.DELETE)
	public ResponseEntity<Kredit> remove(@PathVariable("id") Long id) {
		Optional<Kredit> n = kreditService.findOne(id);
		if (n == null) {
			return new ResponseEntity<Kredit>(HttpStatus.NOT_FOUND);
		}
		kreditService.remove(id);
		return new ResponseEntity<Kredit>(HttpStatus.OK);
	}

	@RequestMapping(path = "", method = RequestMethod.PUT)
	public ResponseEntity<Optional<Kredit>> edit(@RequestBody() Kredit n) {
		Optional<Kredit> kredit = kreditService.findOne(n.getId());
		if (kredit == null) {
			return new ResponseEntity<Optional<Kredit>>(HttpStatus.NOT_FOUND);
		}
		kreditService.save(n);
		return new ResponseEntity<Optional<Kredit>>(HttpStatus.OK);
	}
}
