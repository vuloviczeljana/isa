package app.controller;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import app.model.Banka;
import app.service.BankaService;


@Controller
@RequestMapping(path = "api/banke")
public class BankaController {
	@Autowired
	BankaService bankaService;

	@RequestMapping(path = "/dobavi-sve", method = RequestMethod.GET)
	public ResponseEntity<Iterable<Banka>> dobaviBanke() {
		return new ResponseEntity<Iterable<Banka>>(bankaService.findAll(), HttpStatus.OK);
	}

	@RequestMapping(path = "/{id}", method = RequestMethod.GET)
	public ResponseEntity<Optional<Banka>> dobaviBanku(@PathVariable("id") Long id) {
		Optional<Banka> n = bankaService.findOne(id);
		if (n != null)
			return new ResponseEntity<Optional<Banka>>(n, HttpStatus.OK);
		else
			return new ResponseEntity<Optional<Banka>>(HttpStatus.NOT_FOUND);
	}

	@RequestMapping(path = "", method = RequestMethod.POST)
	public ResponseEntity<Banka> save(@RequestBody() Banka n) {
		bankaService.save(n);
		return new ResponseEntity<Banka>(n, HttpStatus.CREATED);
	}

	@RequestMapping(path = "/{id}", method = RequestMethod.DELETE)
	public ResponseEntity<Banka> remove(@PathVariable("id") Long id) {
		Optional<Banka> n = bankaService.findOne(id);
		if (n == null) {
			return new ResponseEntity<Banka>(HttpStatus.NOT_FOUND);
		}
		bankaService.remove(id);
		return new ResponseEntity<Banka>(HttpStatus.OK);
	}

	@RequestMapping(path = "", method = RequestMethod.PUT)
	public ResponseEntity<Optional<Banka>> edit(@RequestBody() Banka n) {
		Optional<Banka> banka = bankaService.findOne(n.getId());
		if (banka == null) {
			return new ResponseEntity<Optional<Banka>>(HttpStatus.NOT_FOUND);
		}
		bankaService.save(n);
		return new ResponseEntity<Optional<Banka>>(HttpStatus.OK);
	}
}
