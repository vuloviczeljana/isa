package app.service;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import app.model.Banka;
import app.model.Kredit;
import app.repository.KreditRepository;

@Service
public class KreditService {
	@Autowired
	private KreditRepository kreditRepository;
	
	public Iterable<Kredit> findAll(){
		return kreditRepository.findAll();
		}
	
	public Iterable<Kredit> findAll(Pageable pageable){
		return kreditRepository.findAll(pageable);
		
	}
	public Optional<Kredit> findOne(Long id){
		return kreditRepository.findById(id);
		}
	
	public void remove(Long id){
		kreditRepository.deleteById(id);
	}
	public Kredit save(Kredit kredit){
		return kreditRepository.save(kredit);
		
	}
	

}
