package app.service;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import app.controller.KlijentController;
import app.model.Banka;
import app.model.Klijent;
import app.repository.KlijentRepository;

@Service
public class KlijentService {
	@Autowired
	private KlijentRepository klijentRepository;
	
	public Iterable<Klijent> findAll(){
		return klijentRepository.findAll();
		}
	
	public Iterable<Klijent> findAll(Pageable pageable){
		return klijentRepository.findAll(pageable);
		
	}
	public Optional<Klijent> findOne(Long id){
		return klijentRepository.findById(id);
		}
	
	public void remove(Long id){
		klijentRepository.deleteById(id);
	}
	public Klijent save(Klijent klijent){ 
		return klijentRepository.save(klijent);
		
	}
}
