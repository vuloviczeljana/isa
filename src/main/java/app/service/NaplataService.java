package app.service;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;


import app.model.Naplata;
import app.repository.NaplataRepository;

@Service
public class NaplataService {
	@Autowired
	private NaplataRepository naplataRepository;
	
	public Iterable<Naplata> findAll(){
		return naplataRepository.findAll();
		}
	
	public Iterable<Naplata> findAll(Pageable pageable){
		return naplataRepository.findAll(pageable);
		
	}
	public Optional<Naplata> findOne(Long id){
		return naplataRepository.findById(id);
		}
	
	public void remove(Long id){
		naplataRepository.deleteById(id);
	}
	public Naplata save(Naplata naplata){
		return naplataRepository.save(naplata);
		
	}
}
