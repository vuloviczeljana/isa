package app.service;


import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Service;

import app.model.Banka;
import app.repository.BankaRepository;

@Service
public class BankaService {
	@Autowired 
	private BankaRepository bankaRepository;
	
	public Iterable<Banka> findAll(){
		return bankaRepository.findAll();
		}
	

	public Optional<Banka> findOne(Long id){
		return bankaRepository.findById(id);
		}
	
	public void remove(Long id){
		bankaRepository.deleteById(id);
	}
	
	public Banka save(Banka banka){ 
		return bankaRepository.save(banka);
		
	}

}
