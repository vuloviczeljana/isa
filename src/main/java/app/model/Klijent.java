package app.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import org.hibernate.annotations.Type;

@Entity
public class Klijent {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@Column
	@Type(type = "text")
	private String ime;
	private String prezime; 
	private String jmbg; 

	public Klijent() {
		super();
	}

	public Klijent(Long id, String ime, String prezime, String jmbg) {
		super();
		this.id = id;
		this.ime = ime;
		this.prezime = prezime; 
		this.jmbg = jmbg; 
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getIme() {
		return ime;
	}

	public void setIme(String ime) {
		this.ime = ime;
	}
	
	public String getPrezime() {
		return prezime; 
	}
	
	public String setPrezime(String prezime) {
		return prezime; 
	}
	public String getJmbg() {
		return jmbg; 
	}
	public void setPJmbg(String jmbg) {
		this.jmbg = jmbg; 
	}
	
	
	}

