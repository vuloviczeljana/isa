package app.model;

import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import org.hibernate.annotations.Type;

import org.hibernate.annotations.Type;

@Entity
public class Naplata {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	@Column
	@Type(type = "text")
	private Double iznos;
	private LocalDateTime datum; 
	
	
	
	public Naplata() {
		super();
	}

	public Naplata(Long id, Double iznos, LocalDateTime datum) {
		super();
		this.id = id;
		this.iznos = iznos;
	}


	public Long getId() {
		return id;
	}


	public void setId(Long id) {
		this.id = id;
	}


	public Double getIznos() {
		return iznos;
	}


	public void setIznos(Double iznos) {
		this.iznos = iznos;
	}	
	
	public LocalDateTime getDatum(LocalDateTime datum) { 
		return datum; 
	}
	
	public void setDatum(LocalDateTime datum) {
		this.datum = datum; 
	}
	
	}



