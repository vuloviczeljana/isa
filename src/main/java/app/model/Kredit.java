package app.model;

import java.time.LocalDateTime;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
//import javax.persistence.ManyToOne;

import org.hibernate.annotations.Type;

@Entity
public class Kredit {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	@Column(nullable = false)
	private Double iznos; 
	@Column(nullable = false)
	private Double kamata; 
	@Column(nullable = false)
	@Type(type = "date")
	private LocalDateTime datumIzdavanja;
	@Column(nullable = false)
	@Type(type = "date")
	private LocalDateTime rokOtplate;



	public Kredit() {
		super();
	}

	public Kredit(Long id, Double iznos, Double kamata,  LocalDateTime datumIzdavanja, LocalDateTime rokOtplate) {
		super();
		this.id = id;
		this.iznos = iznos; 
		this.kamata = kamata; 
		this.datumIzdavanja = datumIzdavanja;
		this.rokOtplate = rokOtplate;
		
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public LocalDateTime getDatumIzdavanja() {
		return datumIzdavanja;
	}

	public void setDatumIzdavanja(LocalDateTime datumIzdavanja) {
		this.datumIzdavanja = datumIzdavanja;
	}

	public LocalDateTime getRokOtplate() {
		return rokOtplate;
	}

	public void setDatumPrestanka(LocalDateTime rokOtplate) {
		this.rokOtplate = rokOtplate;
	}

}
